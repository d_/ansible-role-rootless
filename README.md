# Rootless Role
A role to set up a rootless environment to have a well configured
shell and various packages not installed on the remote host.

```bash
./run.yml -e pseudo_root_dir=/home/user -i user@hostname,
```

> ⚠ **WARNING**: This role will overwrite your shell configs!!!

## Shell configs explanation
Fish is not set as the default shell. Instead it is run by
`.bash_profile`, so the env vars exported by `.profile` don't need to
be repeated in the `config.fish`. `.bashrc` is just a duplicate of
`.profile` for ansible to pick up the same env vars.

## ncurses explanation
Ncurses is installed early so that fish and nnn can link to it as a
shared library.  `LD_PRELOAD` is set anyways in the shell configs so
that foot terminfo works for other dependent executables that were
already linked to the older ncurses already present on the remote host
such as vim.
