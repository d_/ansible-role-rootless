let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(stdpath('data') . '/plugged')
Plug 'joshdick/onedark.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'ntpeters/vim-better-whitespace'
Plug 'haya14busa/incsearch.vim'
Plug 'jreybert/vimagit'
Plug 'airblade/vim-gitgutter'
Plug 'farmergreg/vim-lastplace'
Plug 'jamessan/vim-gnupg'
Plug 'lambdalisue/suda.vim'
call plug#end()

" Indent with 4 spaces and show linebreaks
set expandtab
set shiftwidth=4
set softtabstop=4

set breakindent
set formatoptions=l
set linebreak
set showbreak=\\

" Have persistent undos
set undofile

" Remember cursor position
autocmd BufReadPost *
\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
\ |   exe "normal! g`\""
\ | endif

" Enable syntax highlighting and use onedark theme
set termguicolors
syntax on
colorscheme onedark


" Use space as leader key
nnoremap <Space> <Nop>
noremap <Space> <Nop>
sunmap <Space>

let mapleader = ' '
map <Leader>a ggVG

" Use <spc>-c to run python on python ft buffers
autocmd FileType python nnoremap <buffer> <leader>c :exec '!python3' shellescape(@%, 1)<cr>

" Make movement based on visual lines
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> 0 g0
noremap <silent> $ g$

for first in ['', 'g', '[', ']']
    for mode in ['n', 'x', 'o']
        exe mode . "noremap " . first . "' " . first . "`"
        exe mode . "noremap " . first . "` " . first . "'"
    endfor
endfor
